/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(4);
        circle.calArea();
        circle.print();
        
        Triangle triangle = new Triangle(4,3);
        triangle.calArea();
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4,3);
        rectangle.calArea();
        rectangle.print();
        
        Shape side = null;
        side = rectangle;
        
        Square square = new Square(2);
        square.calArea();
        square.print();
        
        Shape[] shapes = {circle,triangle,rectangle,square};
        for(int i=0;i<shapes.length;i++){
            System.out.println("----------");
            shapes[i].calArea();
            shapes[i].print();
        }
        
    }
}
