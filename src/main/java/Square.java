/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class Square extends Rectangle{
    public Square(double side){
        super(side,side);
    }

    @Override
    public double calArea() {
        return super.calArea();
    }

    @Override
    public void print() {
        System.out.println("Square: " + "side: " + this.width + " Area: " + this.calArea());
    }
    
}
