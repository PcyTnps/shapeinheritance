/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class Rectangle extends Shape {

    protected double width;
    protected double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double calArea() {
        return width * height;
    }

    @Override
    public void print() {
        System.out.println("Rectangle: " + "width: " + this.width + " height: " + this.height + " Area: " + this.calArea());
    }
    
}
