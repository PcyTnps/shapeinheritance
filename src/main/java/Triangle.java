

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    }
    @Override
    public double calArea(){
        return base*height;
    }
    @Override
    public void print(){
        System.out.println("Triangle: "+"base: "+this.base+" height: "+this.height+" Area: "+this.calArea());
    }
}
