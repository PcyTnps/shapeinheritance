/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class Circle extends Shape{
    private double r;
    static final double pi = 22.0/7;
    
    public Circle(double r){
        this.r = r;
    }
    @Override
    public double calArea(){
        return pi*r*r;
    }
    @Override
    public void print(){
        System.out.println("Circlr: "+"r: "+this.r+" Area: "+this.calArea());
    }

}
